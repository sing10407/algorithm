﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAlgorithm
{
    public class BinarySearch
    {
        public int[] sortedArray;
        public BinarySearch(int[] initArray) {
            sortedArray = initArray;
        }

        public int StartBinarySearch(int value) {
            return StartBinarySearch(0, sortedArray.Length, value);
        }

        public int StartBinarySearch(int start, int end, int value)
        {
            while (start < end)
            {
                int mid = (start + end) / 2;
                if (sortedArray[mid] == value)
                    return mid;
                else if (sortedArray[mid] > value)
                {
                    end = mid - 1;
                }
                else if (sortedArray[mid] < value)
                {
                    start = mid + 1;
                }
            }
            return -1;
        }
    }
}
