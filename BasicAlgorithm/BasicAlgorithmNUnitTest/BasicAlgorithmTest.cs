using BasicAlgorithm;
using NUnit.Framework;

namespace Tests
{
    public class BasicAlgorithmTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(1,0)]
        [TestCase(5, 2)]
        [TestCase(6, 3)]
        [TestCase(8, 5)]
        [TestCase(88, 10)]
        [TestCase(99, -1)]
        [TestCase(10, -1)]
        public void BinarySearch(int input, int expected)
        {
            BinarySearch binarySearch = new BinarySearch(new int[] { 1, 3, 5, 6, 7, 8, 9, 11, 33, 46, 88 });

            int result = binarySearch.StartBinarySearch(input);

            Assert.AreEqual(expected, result);
        }
    }
}